package AtividadePraticaDeUML;

public class CadastraProduto extends CadastraUsuario{

	public String nomeProduto;
	public int tamanhoProduto;
	public int qntEmEstoque;
	public int codigoDeBarras;
	
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	public int getTamanhoProduto() {
		return tamanhoProduto;
	}
	public void setTamanhoProduto(int tamanhoProduto) {
		this.tamanhoProduto = tamanhoProduto;
	}
	public int getQntEmEstoque() {
		return qntEmEstoque;
	}
	public void setQntEmEstoque(int qntEmEstoque) {
		this.qntEmEstoque = qntEmEstoque;
	}
	
	public int getCodigoDeBarras() {
		return codigoDeBarras;
	}
	public void setCodigoDeBarras(int codigoDeBarras) {
		this.codigoDeBarras = codigoDeBarras;
	}
	
	public int estoqueDeterminadaRoupa(int roupaComprada) {
		int qntEstoque = roupaComprada - getQntEmEstoque();
		return qntEstoque;
	}
	
	public String compraramDeterminadaRoupa(String qualRoupa) {
		if(qualRoupa == getNomeProduto());
		return this.nome;
	}
	
	public String roupaCompradaPorUmCliente(String nome) {
		if(getNome() == nome) ;
		return this.nomeProduto;
		  
	}
	
	
}
